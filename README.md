# Array Frame

This project contains
- Design files for the array frame
- List of hardware components
- Schematics
- Source code

![](Images/frame_bottom_front_side.jpg)
![](Images/frame_top_marked.jpg)
