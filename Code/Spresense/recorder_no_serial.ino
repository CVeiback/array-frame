/*
 *  recorder_wav.ino - Recorder example application for WAV(PCM)
 *  Copyright 2018 Sony Semiconductor Solutions Corporation
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <MP.h>
#include <SDHCI.h>
#include <Audio.h>

#include "imu/communication.h"
#include <arch/board/board.h>

SDClass theSD;
AudioClass *theAudio;

#define BoardInit 0  /* Initializing */
#define BoardReady 1   /* Ready to respond */
#define BoardRecording 2   /* Recording */
uint8_t boardMode;

#define BUTTON_PIN PIN_D08
bool buttonPressed = false;
int64_t last_blink = 0;
bool Recording = false;
bool ErrEnd = false;

static const int32_t channels = 8; /* 2, 4, 8) */
static const int32_t frequency = 48000; /* 16, 48, 64, 96, 128, 192 kHz */

const int subcore = 1;
IMUFIFO * imu_fifo =(IMUFIFO *) 0;
  
File myFile;
char filename[100];

#define IMU_FRAME_SIZE (sizeof(magicWord) + sizeof(dataSize) + sizeof(IMUData))
#define BUFFER_SIZE (1024*28)
const uint8_t magicWord[] = {0xFE, 0xED, 0xAB, 0xEE, 0xDE, 0xAD};//, 0xBE, 0xEF
const uint16_t imuSize = sizeof(IMUData);
uint8_t writeBuffer[BUFFER_SIZE];
uint16_t lastWrite = 0;
uint32_t writeSize = 0;

int32_t number;
static const char* fileformat = "Sound%03d.wav";

static void audio_attention_cb(const ErrorAttentionParam *atprm)
{
  puts("Attention!");
  
  if (atprm->error_code >= AS_ATTENTION_CODE_WARNING)
    {
      ErrEnd = true;
   }
}

void setup()
{
  number = 1;
  boardMode = BoardInit;
  
  puts("Initializing LEDs...");
  pinMode(LED0, OUTPUT);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);

  puts("Initializing Serial Port...");
  Serial.begin(115200);

  pinMode(BUTTON_PIN, INPUT_PULLUP);
  buttonPressed = !digitalRead(BUTTON_PIN);
        
  puts("Initializing SD Card...");
  theSD.begin();
  
  puts("Initializing Audio Library...");
  theAudio = AudioClass::getInstance();
  theAudio->begin(audio_attention_cb);

  puts("Setting Recorder Mode...");
  uint8_t input_device = AS_SETRECDR_STS_INPUTDEVICE_MIC;
  int32_t input_gain = 0;
  uint32_t buffer_size = 2*SIMPLE_FIFO_BUF_SIZE;
  bool is_digital = true;
  err_t res = theAudio->setRecorderMode(input_device, input_gain, buffer_size, is_digital);

  puts("Initializing Recorder...");
  theAudio->initRecorder(AS_CODECTYPE_WAV,"/mnt/sd0/BIN",frequency, channels);
  
  puts("Initializing SubCore 1...");
  void * data;
  int8_t msgid = 0;
  int ret = MP.begin(subcore);
  if (ret < 0) {
    printf("MP.begin error = %d\n", ret);
  }
  MP.RecvTimeout(MP_RECV_POLLING);

  Serial.println("Subcore started, waiting: ");
  ret = -1;
  MP.RecvTimeout(500);
  while(!(ret >= 0 && msgid == MSG_FIFO_ADDRESS)) {   
    Serial.print("."); 
    ret = MP.Recv(&msgid, &data, subcore);
  }
  MP.RecvTimeout(MP_RECV_POLLING);
  Serial.println("Pointer received");
  imu_fifo = (IMUFIFO *) data;
  MP.Send(MSG_FIFO_ADDRESS, ACK, subcore);
  
  while(ret == 0) {ret = MP.Recv(&msgid, &data, subcore);}
    
  digitalWrite(LED0, HIGH);
  boardMode = BoardReady;
}

void StartRecording()
{
  if (boardMode != BoardReady)
  {
    Serial.print("Wrong board mode: ");
    Serial.println(boardMode);
    return;
  }
  boardMode = BoardRecording;
  
  puts("Opening file...");
  
  sprintf(filename, fileformat, number);
  while(theSD.exists(filename)) //
  {
    sprintf(filename, fileformat, ++number);    
  }  
  puts(filename);
    
  myFile = theSD.open(filename, FILE_WRITE);
  if (!myFile) 
  {
      puts("File open error");
      ErrEnd = true; return;
  }
  
  puts("Writing header...");
  theAudio->writeWavHeader(myFile);

  writeSize = 0;

  puts("Starting Recorder...");
  theAudio->startRecorder();
  
  digitalWrite(LED1, HIGH);
  Recording = true;
  puts("Recording Started...");
  lastWrite = millis();
}

void StopRecording(bool error = false)
{ 
  if (boardMode != BoardRecording)
  {
    Serial.print("Wrong board mode: ");
    Serial.println(boardMode);
    return;
  }
  err_t err;
  
  puts("Stop Recording...");
  digitalWrite(LED1, LOW);
  theAudio->stopRecorder();
  Recording = false;
  
  puts("Closing File...");
  theAudio->closeOutputFile(myFile);
  
  puts("Recording Stopped...");
  
  boardMode = BoardReady;
}

void loop() 
{
  /* Blink status LED to show that it is alive */
  if (millis()-last_blink > 1000) 
  {
    digitalWrite(LED2, !digitalRead(LED2));
    last_blink = millis();
  }

  /* Start and stop recording with the press of a button */
  bool buttonPressedNow = !digitalRead(BUTTON_PIN);
  if (!buttonPressedNow && buttonPressed) 
  {
    if(Recording) StopRecording(); else StartRecording(); 
  }
  buttonPressed = buttonPressedNow;
  
  /* IMU data available, copy to SD write buffer */
  if (imu_fifo->tail != imu_fifo->head)
  {
    if (Recording) 
    {
      // Copy IMU data to write buffer
      memcpy(&writeBuffer[writeSize], magicWord, sizeof(magicWord)); 
        writeSize += sizeof(magicWord);
      memcpy(&writeBuffer[writeSize], &imuSize, sizeof(imuSize)); 
        writeSize += sizeof(imuSize);
      memcpy(&writeBuffer[writeSize], &imu_fifo->imu[imu_fifo->tail], imuSize); 
        writeSize += imuSize;
    }
        
    imu_fifo->tail = (imu_fifo->tail + 1) % IMU_BUFFER_SIZE;
  }

  /* Copy audio data to SD write buffer */
  if (Recording)
  {
    uint32_t bytesWritten;
    uint16_t tempTime;
    
    err_t err = AUDIOLIB_ECODE_INSUFFICIENT_BUFFER_AREA;

    while(err == AUDIOLIB_ECODE_INSUFFICIENT_BUFFER_AREA)
    {
      err = theAudio->readFrames(&writeBuffer[writeSize], sizeof(writeBuffer) - writeSize, &bytesWritten );
        writeSize += bytesWritten;

      //printf("dSize = %d\n", writeSize);
      if(err == AUDIOLIB_ECODE_INSUFFICIENT_BUFFER_AREA) printf("IBA T = %d\n", millis()-lastWrite);
      
      /* Write combined stream to SD card */
      if (bytesWritten > 0) {
        tempTime = millis();
        if(tempTime-lastWrite > 24) printf("T: %u\n", tempTime - lastWrite);
        lastWrite = tempTime;
        myFile.write(writeBuffer, writeSize);
        writeSize = 0;
      }
    }
    
    if (err != AUDIOLIB_ECODE_OK)
    {
      printf("File End! =%d\n",err);
      StopRecording(true);
    }

  }
  
  if (ErrEnd)
  {
    printf("Error End\n");
    StopRecording(true);
    ErrEnd = false;
  }
}
