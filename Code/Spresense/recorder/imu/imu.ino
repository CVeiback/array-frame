#include <MP.h>
#include <MPU9250.h>
#include <Watchdog.h>
#include "quaternionFilters.h"
#include "communication.h"
#include <SoftwareWire.h>

int ledStatus  = LED3;

#define PIN_SDA 14
#define PIN_SCL 15
SoftwareWire softWire(PIN_SDA, PIN_SCL, false, false);

char debugMsg[200];
#define serialDebug false // Print debug data

#define I2Cclock 100000
#define I2Cport softWire
#define MPU9250_ADDRESS MPU9250_ADDRESS_AD0
#define MPU9250_ID (0x71)
#define MPU9250_MAG_ID (0x48)
MPU9250 myIMU(MPU9250_ADDRESS, I2Cport, I2Cclock);
float rawmx, rawmy, rawmz;
IMUFIFO imuFifo;

bool i2cCheckLineState() {

  sprintf(debugMsg, "Current state sda(%d)=%d, scl(%d)=%d", PIN_SDA, digitalRead(PIN_SDA), PIN_SCL, digitalRead(PIN_SCL));
  Serial.println(debugMsg);

  // I2C bus OK
  if (digitalRead(PIN_SDA) && digitalRead(PIN_SCL)) {
    return true;
  }

  pinMode(PIN_SDA, INPUT_PULLUP);
  pinMode(PIN_SCL, INPUT_PULLUP);

  sprintf(debugMsg, "Current state sda(%d)=%d, scl(%d)=%d", PIN_SDA, digitalRead(PIN_SDA), PIN_SCL, digitalRead(PIN_SCL));
  Serial.println(debugMsg);

  // I2C bus is busy, client keeps SDA high
  if (!digitalRead(PIN_SDA) || !digitalRead(PIN_SCL)) { 
    sprintf(debugMsg, "invalid state sda(%d)=%d, scl(%d)=%d", PIN_SDA, digitalRead(PIN_SDA), PIN_SCL, digitalRead(PIN_SCL));
    Serial.println(debugMsg);

    // Manually cycle the clock until client releases SDA
    pinMode(PIN_SCL, OUTPUT);
    digitalWrite(PIN_SCL, HIGH);
    for (uint8_t a = 0; a < 9; a++) {
      delayMicroseconds(5);
      digitalWrite(PIN_SCL, LOW);
      delayMicroseconds(5);
      digitalWrite(PIN_SCL, HIGH);
      if (digitalRead(PIN_SDA)) { // bus recovered
        sprintf(debugMsg, "Recovered after %d Cycles", a + 1);
        Serial.println(debugMsg);
        break;
      }
    }
    
    // Send start and stop condition to client
    pinMode(PIN_SDA, OUTPUT);
    delayMicroseconds(5);
    digitalWrite(PIN_SDA, LOW);
    delayMicroseconds(5);
    digitalWrite(PIN_SDA, HIGH);
  }

  pinMode(PIN_SCL, INPUT);
  pinMode(PIN_SDA, INPUT);
  pinMode(PIN_SCL, INPUT_PULLUP);
  pinMode(PIN_SDA, INPUT_PULLUP);

  // Double check that the I2C bus is ok
  if (!digitalRead(PIN_SDA) || !digitalRead(PIN_SCL)) { // bus in busy state
    sprintf(debugMsg, "Bus Invalid State, TwoWire() Can't init sda=%d, scl=%d", digitalRead(PIN_SDA), digitalRead(PIN_SCL));
    Serial.println(debugMsg);
    return false; // bus is busy
  }

  return true;
}

void setup()
{
  int ret = MP.begin();
  MP.RecvTimeout(MP_RECV_POLLING);

  Serial.begin(115200);
  Serial.println("Subcore 1 started");

  i2cCheckLineState();

  softWire.begin();

  byte c = myIMU.readByte(MPU9250_ADDRESS, WHO_AM_I_MPU9250);

  if (c == MPU9250_ID)
  {
    myIMU.MPU9250SelfTest(myIMU.selfTest);
    
    if(serialDebug)
    {
      Serial.print(F("x-axis self test: acceleration trim within : "));
      Serial.print(myIMU.selfTest[0], 1); Serial.println("% of factory value");
      Serial.print(F("y-axis self test: acceleration trim within : "));
      Serial.print(myIMU.selfTest[1], 1); Serial.println("% of factory value");
      Serial.print(F("z-axis self test: acceleration trim within : "));
      Serial.print(myIMU.selfTest[2], 1); Serial.println("% of factory value");
      Serial.print(F("x-axis self test: gyration trim within : "));
      Serial.print(myIMU.selfTest[3], 1); Serial.println("% of factory value");
      Serial.print(F("y-axis self test: gyration trim within : "));
      Serial.print(myIMU.selfTest[4], 1); Serial.println("% of factory value");
      Serial.print(F("z-axis self test: gyration trim within : "));
      Serial.print(myIMU.selfTest[5], 1); Serial.println("% of factory value");
    }
    
    myIMU.calibrateMPU9250(myIMU.gyroBias, myIMU.accelBias);

    myIMU.initMPU9250();
    Serial.println("MPU9250 online");

     byte d = myIMU.readByte(AK8963_ADDRESS, WHO_AM_I_AK8963);

    if (d != MPU9250_MAG_ID)
    {
      Serial.print("Could not connect to AK8963: 0x");
      Serial.println(d, HEX);
      
      // Communication failed, stop here      
      Serial.println(F("Communication failed, abort!"));
      Serial.flush();
      abort();
    }

    myIMU.initAK8963(myIMU.factoryMagCalibration);
    
    Serial.println("AK8963 online");

    if (serialDebug)
    {
      Serial.println("Calibration values: ");
      Serial.print("X-Axis factory sensitivity adjustment value ");
      Serial.println(myIMU.factoryMagCalibration[0], 2);
      Serial.print("Y-Axis factory sensitivity adjustment value ");
      Serial.println(myIMU.factoryMagCalibration[1], 2);
      Serial.print("Z-Axis factory sensitivity adjustment value ");
      Serial.println(myIMU.factoryMagCalibration[2], 2);
    }
    
    // Get sensor resolutions, only need to do this once
    myIMU.getAres();
    myIMU.getGres();
    myIMU.getMres();

    if (serialDebug)
    {
      Serial.print("Resolution value: ");
      Serial.println(myIMU.mRes, 2);
    
      Serial.println("AK8963 mag biases (mG)");
      Serial.println(myIMU.magBias[0]);
      Serial.println(myIMU.magBias[1]);
      Serial.println(myIMU.magBias[2]);
  
      Serial.println("AK8963 mag scale (mG)");
      Serial.println(myIMU.magScale[0]);
      Serial.println(myIMU.magScale[1]);
      Serial.println(myIMU.magScale[2]);

      Serial.println("Magnetometer:");
      Serial.print("X-Axis sensitivity adjustment value ");
      Serial.println(myIMU.factoryMagCalibration[0], 2);
      Serial.print("Y-Axis sensitivity adjustment value ");
      Serial.println(myIMU.factoryMagCalibration[1], 2);
      Serial.print("Z-Axis sensitivity adjustment value ");
      Serial.println(myIMU.factoryMagCalibration[2], 2);
    }
  }
  else
  {
    Serial.print("Could not connect to MPU9250: 0x");
    Serial.println(c, HEX);

    // Communication failed, reboot
    Serial.println(F("Communication failed, abort!"));
    Serial.flush();

    // Restart processor to try again
    Watchdog.begin();
    Watchdog.start(1);
    while (true) {
      ;
    }
    abort();
  }

  // Share memory with main core
  Serial.println("Sending FIFO address...");
  
  uint32_t data;
  int8_t msgid = 0;
  ret = -1;
  MP.RecvTimeout(500);
  while (!(ret >= 0 && msgid == MSG_FIFO_ADDRESS && data == ACK)) {
    MP.Send(MSG_FIFO_ADDRESS, &imuFifo);
    Serial.print(".");
    ret = MP.Recv(&msgid, &data);
  }
  MP.RecvTimeout(MP_RECV_POLLING);
  Serial.println("ACK received");

  pinMode(ledStatus, OUTPUT);
  digitalWrite(ledStatus, HIGH);
}

void loop()
{
  if (myIMU.readByte(MPU9250_ADDRESS, INT_STATUS) & 0x01)
  {
    myIMU.readAccelData(myIMU.accelCount);

    myIMU.ax = (float)myIMU.accelCount[0] * myIMU.aRes;
    myIMU.ay = (float)myIMU.accelCount[1] * myIMU.aRes; 
    myIMU.az = (float)myIMU.accelCount[2] * myIMU.aRes;

    myIMU.readGyroData(myIMU.gyroCount); 

    myIMU.gx = (float)myIMU.gyroCount[0] * myIMU.gRes;
    myIMU.gy = (float)myIMU.gyroCount[1] * myIMU.gRes;
    myIMU.gz = (float)myIMU.gyroCount[2] * myIMU.gRes;

    myIMU.readMagData(myIMU.magCount);  

    rawmx = (float)myIMU.magCount[0] * myIMU.mRes * myIMU.factoryMagCalibration[0];
    rawmy = (float)myIMU.magCount[1] * myIMU.mRes * myIMU.factoryMagCalibration[1] ;
    rawmz = (float)myIMU.magCount[2] * myIMU.mRes * myIMU.factoryMagCalibration[2];

    // Use hardcoded calibration
    myIMU.mx = (rawmx - myIMU.magBias[0]) * myIMU.magScale[0];
    myIMU.my = (rawmy - myIMU.magBias[1]) * myIMU.magScale[1];
    myIMU.mz = (rawmz - myIMU.magBias[2]) * myIMU.magScale[2];

  } 
  
  myIMU.updateTime();

  // Sensors x (y)-axis of the accelerometer is aligned with the y (x)-axis of
  // the magnetometer; the magnetometer z-axis (+ down) is opposite to z-axis
  // (+ up) of accelerometer and gyro! We have to make some allowance for this
  // orientationmismatch in feeding the output to the quaternion filter. For the
  // MPU-9250, we have chosen a magnetic rotation that keeps the sensor forward
  // along the x-axis just like in the LSM9DS0 sensor. This rotation can be
  // modified to allow any convenient orientation convention. This is ok by
  // aircraft orientation standards! Pass gyro rate as rad/s
  MahonyQuaternionUpdate(myIMU.ax, myIMU.ay, myIMU.az, myIMU.gx * DEG_TO_RAD,
                         myIMU.gy * DEG_TO_RAD, myIMU.gz * DEG_TO_RAD, myIMU.my,
                         myIMU.mx, myIMU.mz, myIMU.deltat);

  myIMU.delt_t = millis() - myIMU.count;

  if (myIMU.delt_t > 10)
  {
    myIMU.count = millis();
    myIMU.sumCount = 0;
    myIMU.sum = 0;

    // Read temperature and convert to Celsius
    myIMU.tempCount = myIMU.readTempData();
    myIMU.temperature = ((float) myIMU.tempCount) / 333.87 + 21.0;

    // Convert to yaw, pitch, roll
    const float * q = getQ();
    myIMU.yaw   = atan2(2.0f * (q[1]*q[2] + q[0]*q[3]), q[0]*q[0] + q[1]*q[1] - q[2]*q[2] - q[3]*q[3]);
    myIMU.pitch = -asin(2.0f * (q[1]*q[3] - q[0]*q[2]));
    myIMU.roll  = atan2(2.0f * (q[0]*q[1] + q[2]*q[3]), q[0]*q[0] - q[1]*q[1] - q[2]*q[2] + q[3]*q[3]);
    myIMU.pitch *= RAD_TO_DEG;
    myIMU.yaw   *= RAD_TO_DEG;

    // Declination of LiU
    // - http://www.ngdc.noaa.gov/geomag-web/#declination
    myIMU.yaw  -= 5.53;
    myIMU.roll *= RAD_TO_DEG;

    if (((imuFifo.head + 1) % IMU_BUFFER_SIZE) != imuFifo.tail)
    {
      imuFifo.imu[imuFifo.head].timestamp = millis();
      imuFifo.imu[imuFifo.head].temperature = myIMU.temperature;
      imuFifo.imu[imuFifo.head].gyro[0] = myIMU.gx;
      imuFifo.imu[imuFifo.head].gyro[1] = myIMU.gy;
      imuFifo.imu[imuFifo.head].gyro[2] = myIMU.gz;
      imuFifo.imu[imuFifo.head].accelerometer[0] = myIMU.ax;
      imuFifo.imu[imuFifo.head].accelerometer[1] = myIMU.ay;
      imuFifo.imu[imuFifo.head].accelerometer[2] = myIMU.az;
      imuFifo.imu[imuFifo.head].magnetometer[0] = rawmx;
      imuFifo.imu[imuFifo.head].magnetometer[1] = rawmy;
      imuFifo.imu[imuFifo.head].magnetometer[2] = rawmz;
      imuFifo.imu[imuFifo.head].orientation[0] = myIMU.yaw;
      imuFifo.imu[imuFifo.head].orientation[1] = myIMU.pitch;
      imuFifo.imu[imuFifo.head].orientation[2] = myIMU.roll;
      
      imuFifo.head = (imuFifo.head + 1) % IMU_BUFFER_SIZE;      
      if (imuFifo.head == 0) digitalWrite(ledStatus, !digitalRead(ledStatus)); 
    }
  }
}
