function [audio, data] = readData(name, path)
%READDATA Read recorded sound and IMU data.
% name    Name of files excluding extension
% path    Path to read combined file from.

%% Load sound
filename = fullfile(path, [name '.wav']);

[audio, fs] = audioread(filename);

% Map channels to positions
audio = audio(:,[5 7 4 1 2 3 8 6]);

t = (1:size(audio,1))/fs*1000;
figure; title('Microphone signals'); 
for i=1:8;subplot(2,4,i); plot(t,audio(1:end,i));title(num2str(i));end

%%
ti = [650 71500];
tc = t(t>= ti(1) & t <= ti(end));
sndc = audio(t>= ti(1) & t <= ti(end),:);
for i=1:8;subplot(2,4,i); plot(tc,sndc(1:end,i));title(num2str(i));end
%% IMU Data
filename = fullfile(path, [name '.bin']);

% Load entire file first for faster parsing
f = fopen(filename);
D = uint8(fread(f, inf, 'uint8=>uint8')');
fclose(f);

% Init data structures for parsing
struct_size = 64;
N = length(D)/struct_size;
timestamp = zeros(N,1);
temperature = zeros(N,1);
gyro = zeros(N,3);
accelerometer = zeros(N,3);
magnetometer = zeros(N,3);
orientation = zeros(N,3);
          
% Parse data
p = 1;
for j = 1:N
  if mod(j,500) == 0; clc; disp([num2str(round(j/N*100,0)) '%']); end
  
  timestamp(j,:) = typecast(D(p +(0:7)), 'uint64'); p = p + 8;
  temperature(j,:) =  typecast(D(p +(0:3)), 'single'); p = p + 4;
  gyro(j,:) = typecast(D(p +(0:11)), 'single'); p = p + 12;
  accelerometer(j,:) = typecast(D(p +(0:11)), 'single'); p = p + 12;
  magnetometer(j,:) = typecast(D(p +(0:11)), 'single'); p = p + 12;
  orientation(j,:) = typecast(D(p +(0:11)), 'single'); p = p + 12;
  p = p + 4; % Dummy
end

data = struct('Timestamp', timestamp, ...
              'Temperature', temperature, ...
              'Gyro', gyro, ...
              'Accelerometer', accelerometer, ...
              'Magnetometer', magnetometer, ...
              'Orientation', orientation);

% Sync with sound recording (hopefully not needed
timestamp = timestamp - timestamp(1);

% Remove outliers if missing data
timestampf = filloutliers(timestamp,'linear','movmedian',1000);
temperaturef = filloutliers(temperature,'linear','movmedian',1000);
gyrof = filloutliers(gyro,'linear','movmedian',1000);
accelerometerf = filloutliers(accelerometer,'linear','movmedian',1000);
magnetometerf = filloutliers(magnetometer,'linear','movmedian',1000);
orientationf = filloutliers(orientation,'linear','movmedian',1000);

 %% Plot data

figure; title('Raw')
subplot(2,2,1); plot(timestamp/1000,orientation); title('Orientation'); xlabel('Time [s]'); ylabel('Attitude [deg]');
subplot(2,2,2); plot(timestamp/1000,accelerometer); title('Accelerometer'); xlabel('Time [s]'); ylabel('Acceleration [g]');
subplot(2,2,3); plot(timestamp/1000,magnetometer);title('Magnetometer'); xlabel('Time [s]'); ylabel('Magnetic Field [gauss]');
subplot(2,2,4); plot(timestamp/1000,gyro);title('Gyro'); xlabel('Time [s]'); ylabel('Angular Velocity [deg/s]');

figure;title('Outliers removed');
subplot(2,2,1); plot(timestamp/1000,orientationf); title('Orientation'); xlabel('Time [s]'); ylabel('Attitude [deg]');
subplot(2,2,2); plot(timestamp/1000,accelerometerf); title('Accelerometer');xlabel('Time [s]'); ylabel('Acceleration [g]');
subplot(2,2,3); plot(timestamp/1000,magnetometerf);title('Magnetometer');xlabel('Time [s]'); ylabel('Magnetic Field [gauss]');
subplot(2,2,4); plot(timestamp/1000,gyrof);title('Gyro'); xlabel('Time [s]'); ylabel('Angular Velocity [deg/s]');