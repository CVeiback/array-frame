// Load Wi-Fi library
#include <WiFi.h>
#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include "esp_log.h"

// Replace with your network credentials
const char* ssid     = "MicHub";
const char* password = "0123456789";

int lastBlink;
int blinkState;

// Set web server port number to 80
WiFiServer server(80);

// Variable to store the HTTP request
String header;

// Auxiliar variables to store the current output state
String recordingState = "off";
String boardState = "not responding";

// Assign output variables to GPIO pins
const int ledPin = 13;

#define BUF_SIZE (255)
uint8_t buf[BUF_SIZE];

#define BoardInit 0  /* Initializing */
#define BoardReady 1   /* Ready to respond */
#define BoardRecording 2   /* Recording */
#define BoardTransmitting 3   /* Transmitting */
#define BoardError 4     /* Error occurred, restart */

/*
// Set your Static IP address
IPAddress local_IP(192, 168, 1, 184);
// Set your Gateway IP address
IPAddress gateway(192, 168, 1, 1);

IPAddress subnet(255, 255, 0, 0);
*/

void setup() {
  Serial.begin(115200);
  
  //Serial1.begin(1843200);
  Serial1.begin(2000000);
  
  Serial1.write(2);// Flush old data
  while(Serial1.available() > 0) {Serial1.read();}

  // Initialize the output variables as outputs
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
  lastBlink = 0;
  blinkState = 0;
  
  connect();

  server.begin();

  readMode();
}

void connect() {
  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
/*
  if (!WiFi.config(local_IP, gateway, subnet)) {
    Serial.println("STA Failed to configure");
  }
  */
  
  WiFi.begin(ssid, password);
  WiFi.setSleep(false); 
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void readMode() {
  // Flush
  while(Serial1.available() > 0) {Serial1.read();}
  
  Serial1.println("status");
  int reqtime = millis();
  while(Serial1.available() == 0 && millis()-reqtime < 1000) { }
  
  Serial.println(Serial1.available());
  
  if (Serial1.available() == 0) {
    boardState = "not responding";
  } else {    
    byte boardMode = Serial1.read();
    Serial.println(boardMode);
    
    switch(boardMode) {
      case BoardInit: boardState = "initializing"; break;
      case BoardReady: boardState = "ready"; break;
      case BoardRecording: boardState = "recording"; break;
      case BoardTransmitting: boardState = "transmitting"; break;
      case BoardError: boardState = "in error mode"; break;   
      default: boardState = "non-responsive"; break;  
    }
  }

  // Flush
  while(Serial1.available() > 0) {Serial1.read();}
}

void loop() {
  if (boardState == "ready") 
  {
    digitalWrite(ledPin, HIGH);
    lastBlink = 0;
    blinkState = 0;
  } else if (boardState != "recording") {
    digitalWrite(ledPin, LOW);
    lastBlink = 0;
    blinkState = 0;
  } else { /* Recording */
    if (blinkState == 0) {
      digitalWrite(ledPin, LOW);
      lastBlink = millis();
      blinkState = 1;
    }  else if (blinkState == 1 && millis()-lastBlink > 1000) {
      digitalWrite(ledPin, HIGH);
      lastBlink = millis();
      blinkState = 2;
    }  else if (blinkState == 2 && millis()-lastBlink > 1000) {
      digitalWrite(ledPin, LOW);
      lastBlink = millis();
      blinkState = 1;
    }
  }
  if (!WiFi.isConnected()) {
    Serial.print("Reconnecting...");
    connect();
  }
  WiFiClient client = server.available();   // Listen for incoming clients
  if (client && client.connected()) {       // If a new client connects,
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        //Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
                   
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            readMode();
            
            
            int i = header.indexOf("/dl/");
            if (i >= 0 && boardState == "ready") {
              int s = header.indexOf("\n",i);
              String dlfile = header.substring(i+4,s-1);
              s = dlfile.indexOf(" ");
              if (s >= 0) {
                dlfile = dlfile.substring(0,s);
              }
              String command = "get " + dlfile;
              
              Serial.println(dlfile);

              // Flush old data
              while(Serial1.available() > 0) {Serial1.read();}
              
              Serial1.println(command);
              String dlsize = "";
              while(true)
              {
                if (Serial1.available() > 0)
                {
                  char c = Serial1.read();
                  if (c == '\n')
                  {
                    break;
                  } else if (isDigit(c)) {
                    dlsize += c;
                  }
                }
              }
              int idlsize = dlsize.toInt();
              Serial.println(idlsize);
              
              client.println("HTTP/1.1 200 OK");
              client.println("Content-type:binary/audio");
              client.println("Content-Length: " + dlsize);
              client.println("Accept-Ranges: bytes");
              client.println("Connection: close");
              client.println("");
              
              int count = 0;
              int m = 0;
              int timestamp = millis();
              while(count < idlsize)
              {
                int left = idlsize - count;
                int expected = (left > BUF_SIZE ? BUF_SIZE : left);
                
                int n = Serial1.available();
                if (n > m)
                {
                  timestamp = millis();
                  m = n;
                }
                
                if (n == expected)
                {                
                  n = Serial1.readBytes(buf, n);
                  Serial1.write(1);
                  client.write(buf,n);
                  count += n;
                  Serial.print(count);
                  Serial.print(", ");
                  m = 0;
                } 
                else if (millis() - timestamp > 100)
                {
                  // Flush old data
                  while(Serial1.available() > 0) {Serial1.read();}
                  Serial1.write(0);
                  Serial.println("Resend request");                  
                }
              }

              Serial.println("Done");
            } else {
              bool printList = false;
              
              // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
              // and a content-type so the client knows what's coming, then a blank line:
              client.println("HTTP/1.1 200 OK");
              client.println("Content-type:text/html");
              client.println("Connection: close");
              client.println();

              // turns the GPIOs on and off
              if (header.indexOf("GET /rec/on") >= 0 && boardState == "ready") {
                Serial.println("Start Recording");
                Serial1.println("start");
                recordingState = "on";
                delay(1000);
                readMode();
              } else if (header.indexOf("GET /rec/off") >= 0 && boardState == "recording") {
                Serial.println("Stop Recording");
                Serial1.println("stop");
                recordingState = "off";
                delay(1000);
                readMode();
              } else if (header.indexOf("GET /list") >= 0 && boardState == "ready") {
                Serial.println("List");
                printList = true;
              } 

              if(printList) 
              {
                // Flush old data
                while(Serial1.available() > 0) {Serial1.read();}
                
                Serial1.println("list");
              }

              // Display the HTML web page
              client.println("<!DOCTYPE html><html>");
              client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
              client.println("<link rel=\"icon\" href=\"data:,\">");
              // CSS to style the on/off buttons
              // Feel free to change the background-color and font-size attributes to fit your preferences
              client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
              client.println("a:link {text-decoration: none;}");
              client.println(".button { background-color: white; border: none; color: red; padding: 16px 40px;");
              client.println("text-decoration: none; font-size: 80px; margin: 2px; cursor: pointer;}");
              client.println(".button2 {background-color: white; color: black;}");
              client.println(".button4 {background-color: white; color: green;}");
              client.println(".button3 {background-color: #white; color: black; font-size: 30px;}</style></head>");

              // Web Page Heading
              client.println("<body><h1>Audio Recording Headset</h1>");

              // Display current state, and ON/OFF buttons for GPIO 26
              client.println("<p>Headset is " + boardState + "</p>");
              // If the recordingState is off, it displays the ON button
              if (boardState == "ready") {
                client.println("<p><a href=\"/rec/on\"><button class=\"button\">&#x25CF;</button></a></p>");
              } else if (boardState == "recording") {
                client.println("<p><a href=\"/rec/off\"><button class=\"button button2\">&#x25A0;</button></a></p>");
              }
              client.println("<p><a href=\"/\"><button class=\"button button4\">&#8635;</button></a></p>");
              client.println("<p><a href=\"/list\"><button class=\"button button3\">List files</button></a></p>");

              if (printList)
              {
                char c;
                String filename = "";
                while(true) {
                  if (Serial1.available() > 0) 
                  {
                    c = Serial1.read();
                    Serial.print(c);
                    if ((char) c == '\n')
                    {
                      if (filename.length() == 0) {  Serial.println("Broken out"); break;}
  
                      String nameonly = filename.substring(0,filename.indexOf('\t'));
                      String filesize = "\t" + filename.substring(filename.indexOf('\t')) + " kb";
                      client.println("<p><a href=\"/dl/" + nameonly + "\">" + nameonly + "</a>" + filesize + "</p>");
                      filename = "";
                    } 
                    else if (c != '\r')
                    {
                      filename += (char) c;
                    }
                  }
                }
              }
              client.println("</body></html>");

            }

            // The HTTP response ends with another blank line
            client.println();
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}
