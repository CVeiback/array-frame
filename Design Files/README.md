Array Frame
-----------

This directory contains the 3D design files for the array frame. 

An array frame consists of:
1 x Glasses
1 x Platform
4 x Connector
8 x Fixtures

Additionally, it contains the STL files for the components for direct import into a 3D printing software.

The array frame is designed to allow mounting
- a Spresense Extension board (mathcing pins)
- an AdaFruit Huzzah32 (matching pins)
- a battery (e.g. velcro)
- a small breadboard (double-sided tape)
- 8 Knowles SPH0690LM4H-1 microphones in the fixtures

All pieces of the array frame are attachable/detachable using a wedge/hole connector interface.
Microphones are e.g. attached to the fixtures using glue and routing wires through paths in the fixture.
A high-performance SD card is needed to record the data. Too much latency in the SD card will cause buffer overflow errors.