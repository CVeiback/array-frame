function [audioBytes, dataBytes] = splitFile(name, outPath, inPath, magicWord, show)
%SPLITFILE Splits a combined sound and data file, by searching for a magic
%word that marks a data block and extract them into a separate file.
% name      Name of file excluding extension
% outPath   Path to place resulting wav and bin files
% inPath    Path to read combined file from.
% magicWord Determine magic word (default fe ed ab ee de ad)
% show      Show progress in console (default true).

if nargin < 5 || isempty(show); show = true; end
  if nargin < 4 || isempty(magicWord) 
    magicWord = hex2dec({'FE', 'ED', 'AB', 'EE', 'DE', 'AD'})'; %, 'BE', 'EF'
  end
  if nargin < 3 || isempty(inPath); inPath = 'E:\'; end
  if nargin < 2 || isempty(outPath); outPath = 'D:\Data\AudioRecorder\'; end
  
  if ~exist(outPath); mkdir(outPath); end
  
  inFilename = fullfile(inPath,[name '.wav']);
  outFilename = fullfile(outPath,[name '.wav']);
  outData = fullfile(outPath,[name '.bin']);
  i = 0;
  while exist(outFilename, 'file') || exist(outData, 'file')
    i = i + 1;
    outFilename = fullfile(outPath,[name '_' num2str(i) '.wav']);
    outData = fullfile(outPath,[name '_' num2str(i) '.bin']);
  end
  batchSize = 1024 * 1024;
  sizeFieldSize = 2;
  %frameSize = length(magicWord) + sizeFieldSize + structSize;
  
  fi = fopen(inFilename);
  fa = fopen(outFilename, 'w');
  fd = fopen(outData, 'w');
  
  fileInfo = dir(filename);
  totalBytes = fileInfo.bytes;
  audioBytes = 0;
  dataBytes = 0;
  
  D = [];
  while(true)
    if show
        clc;
        disp(['Processed: ' num2str(round((100*audioBytes+dataBytes)/totalBytes,1)) '%']);
    end
    
    D = [D fread(fi, batchSize, 'uint8=>uint8')'];
    
    ind = strfind(D, magicWord);
    
    if isempty(ind)
        fwrite(fa, D(1:end-length(magicWord)));
        audioBytes = audioBytes + length(D(1:end-length(magicWord)));
        D(1:end-length(magicWord)) = [];
    else
        p = 1;
        for i = 1:length(ind)-1
          fwrite(fa, D(p:ind(i)-1));      
          audioBytes = audioBytes + length(D(p:ind(i)-1));
          p = ind(i) + length(magicWord);

          frame_size = sum(double(D([p p+1])).*[1 256]); 
          p = p + sizeFieldSize;

          fwrite(fd, D(p + (0:frame_size-1)));
          dataBytes = dataBytes + frame_size;
          p = p + frame_size;
        end
        fwrite(fa, D(p:ind(end)-1)); 
        audioBytes = audioBytes + length(D(p:ind(end)-1));
        p = ind(end);
        
        q = ind(end);
        if length(D) - q > length(magicWord) + sizeFieldSize
            q = q + length(magicWord);
            frame_size = sum(double(D([q q+1])).*[1 256]); 
            q = q + 2;
            if length(D) - q > frame_size
              fwrite(fd, D(q + (0:frame_size-1)));
              dataBytes = dataBytes + frame_size;
              p = q + frame_size;
            else
                D(1:p-1) = [];
                continue;
            end
        else
            D(1:p-1) = [];
            continue;
        end
        if p < length(D) - length(magicWord) + 1 
            fwrite(fa, D(p:end-length(magicWord)));
            audioBytes = audioBytes + length(D(p:end-length(magicWord)));
            
            D(1:end-length(magicWord)) = [];
        end
    end
    if feof(fi) == 1
        fwrite(fa, D);
        audioBytes = audioBytes + length(D);
        D = [];
        if show
            clc;
            disp(['Processed: 100%']);
        end
        break;
    end
  end
  fclose(fi);
  fclose(fa);
  fclose(fd);
end

