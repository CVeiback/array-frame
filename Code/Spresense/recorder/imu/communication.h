#define IMU_BUFFER_SIZE 32
#define IMU_HALF_BUFFER_SIZE (IMU_BUFFER_SIZE/2)

#define MSG_FIFO_ADDRESS 1

#define ACK 1
#define NACK (-1)

typedef struct IMUData {
  uint64_t timestamp;
  float temperature;
  float gyro[3]; // z, y, x
  float accelerometer[3]; // x, y, z
  float magnetometer[3]; // x, y, z
  float orientation[3]; // Yaw, pitch, roll
} IMUData;

typedef struct IMUFIFO {
  IMUData imu[IMU_BUFFER_SIZE];
  int head = 0;
  int tail = 0;
} IMUFIFO;
